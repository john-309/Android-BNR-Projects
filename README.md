Android-BNR-Projects

The exercises/projects in this repo were created using the following book:

[Android Programming: Big Nerd Ranch Guide](https://www.bignerdranch.com/books/android-programming-the-big-nerd-ranch-guide/)


The MASTER branch contains the code built from chapter 1. Since every following chapter builds on top of the last, additional branches were created to display the code added for every subsequent chapter.
